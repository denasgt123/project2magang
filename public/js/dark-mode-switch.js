/*!
 * Dark Mode Switch v1.0.1 (https://github.com/coliff/dark-mode-switch)
 * Copyright 2021 C.Oliff
 * Licensed under MIT (https://github.com/coliff/dark-mode-switch/blob/main/LICENSE)
 */
let darkSwitchDesktop = $('#darkSwitch-desktop');
let darkSwitchMobile = $('#darkSwitch-mobile');
let darkThemeSelected;
$(document).ready(function () {
	initTheme();
	if (currentRouteName === "profile") {
		darkSwitchDesktop.on('click', function (event) {
			// initTheme();
			changeTheme();
		});

		darkSwitchMobile.on('click', function (event) {
			// initTheme();
			changeTheme();
		});
	}
});
// let darkSwitch = document.getElementById("darkSwitch");
// window.addEventListener("load", function () {
//   if (darkSwitch) {
//     initTheme();
//     darkSwitch.addEventListener("change", function () {
//       resetTheme();
//     });
//   }
// });

/**
 * Summary: function that adds or removes the attribute 'data-theme' depending if
 * the switch is 'on' or 'off'.
 *
 * Description: initTheme is a function that uses localStorage from JavaScript DOM,
 * to store the value of the HTML switch. If the switch was already switched to
 * 'on' it will set an HTML attribute to the body named: 'data-theme' to a 'dark'
 * value. If it is the first time opening the page, or if the switch was off the
 * 'data-theme' attribute will not be set.
 * @return {void}
 */
function initTheme() {
	darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
	// console.log(darkThemeSelected);
	if(darkThemeSelected) {
		$('body').attr("data-theme", "dark");
		if (currentRouteName === "profile") {
			// $(".header-section").attr("data-theme", "dark");
			// $(".highlight-thumbnail").attr("data-theme", "dark");
			// $(".post-section").attr("data-theme", "dark");
			// $(".navbar-mobile-section").attr("data-theme", "dark");
			// $(".nav-link").attr("data-theme", "dark");
			darkSwitchDesktop.html('<i class="fa-solid fa-sun"></i> Lightmode');
			darkSwitchMobile.html('<i class="fs-3 fa-solid fa-sun align-self-center pb-1"></i><p class="navbar-mobile-text mb-2 text-center">Light Mode</p>');
		} else if(currentRouteName === "photos" || currentRouteName === "videos") {

		} else if (currentRouteName === "login" || currentRouteName === "register") {
			
		}
	} else {
		$('body').removeAttr("data-theme");
		if (currentRouteName === "profile") {
			// $(".header-section").removeAttr("data-theme");
			// $(".highlight-thumbnail").removeAttr("data-theme");
			// $(".post-section").removeAttr("data-theme");
			// $(".navbar-mobile-section").removeAttr("data-theme");
			// $(".nav-link").removeAttr("data-theme");
			darkSwitchDesktop.html('<i class="fa-solid fa-moon"></i> Nightmode');
			darkSwitchMobile.html('<i class="fs-3 fa-solid fa-moon align-self-center pb-1"></i><p class="navbar-mobile-text mb-2 text-center">Night Mode</p>');
		} else if(currentRouteName === "photos" || currentRouteName === "videos") {

		} else if (currentRouteName === "login" || currentRouteName === "register") {
			
		}
	}
}
// function initTheme() {
// 	var darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
//   	darkSwitch.checked = darkThemeSelected;
//   	darkThemeSelected ? document.body.setAttribute("data-theme", "dark") : document.body.removeAttribute("data-theme");
// }

/**
 * Summary: resetTheme checks if the switch is 'on' or 'off' and if it is toggled
 * on it will set the HTML attribute 'data-theme' to dark so the dark-theme CSS is
 * applied.
 * @return {void}
 */
function changeTheme() {
	darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
	console.log(darkThemeSelected);
	if(darkThemeSelected) {
		localStorage.removeItem("darkSwitch");
		initTheme();
		console.log('profile changed not');
	} else {
		localStorage.setItem("darkSwitch", "dark");
		initTheme();
		console.log('profile changed selected');
	}
}

// function resetTheme() {
// 	if (darkSwitch.checked) {
// 		document.body.setAttribute("data-theme", "dark");
// 		localStorage.setItem("darkSwitch", "dark");
// 	} else {
// 		document.body.removeAttribute("data-theme");
// 		localStorage.removeItem("darkSwitch");
// 	}
// }