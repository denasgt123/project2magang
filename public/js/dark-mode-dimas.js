/*!
 * Dark Mode Switch v1.0.1 (https://github.com/coliff/dark-mode-switch)
 * Copyright 2021 C.Oliff
 * Licensed under MIT (https://github.com/coliff/dark-mode-switch/blob/main/LICENSE)
 */
jQuery(document).ready(function () {
    initTheme();
    initTheme2();
    if (localStorage.getItem("darkSwitch") == "dark" || localStorage.getItem("darkSwitch2") == "dark") {
        document.body.setAttribute("data-theme","dark");
    }
})
var darkSwitch=document.getElementById("darkSwitch");
window.addEventListener("load",(function(){
    if(darkSwitch){
        initTheme();
        darkSwitch.addEventListener("change",(function(){
            resetTheme()
        }))
    }
}));

function initTheme(){
    var darkThemeSelected=localStorage.getItem("darkSwitch")!==null&&localStorage.getItem("darkSwitch")==="dark";
    darkSwitch.checked=darkThemeSelected;
    darkThemeSelected?document.body.setAttribute("data-theme","dark"):document.body.removeAttribute("data-theme")
    if (localStorage.getItem("darkSwitch") == "dark") {
        $('#qrcode h2').css('color','#d2d0d0');
        $('#edit_staff h2').css('color','#d2d0d0');
    }
}

function resetTheme(){
    if(darkSwitch.checked){
        document.body.setAttribute("data-theme","dark");
        localStorage.setItem("darkSwitch","dark")
        $('#qrcode h2').css('color','#d2d0d0');
        $('#edit_staff h2').css('color','#d2d0d0');
    }else{
        $('#qrcode h2').css('color','#000');
        $('#edit_staff h2').css('color','#000');
        document.body.removeAttribute("data-theme");
        localStorage.removeItem("darkSwitch");
        localStorage.removeItem("darkSwitch2")
    }
}

var darkSwitch2=document.getElementById("darkSwitch2");
window.addEventListener("load",(function(){
    if(darkSwitch2){
        initTheme2();
        darkSwitch2.addEventListener("change",(function(){
            resetTheme2()
        }))
    }
}));

function initTheme2(){
    var darkThemeSelected=localStorage.getItem("darkSwitch2")!==null&&localStorage.getItem("darkSwitch2")==="dark";
    darkSwitch2.checked=darkThemeSelected;
    darkThemeSelected?document.body.setAttribute("data-theme","dark"):document.body.removeAttribute("data-theme")
}

function resetTheme2(){
    if(darkSwitch2.checked){
        document.body.setAttribute("data-theme","dark");
        localStorage.setItem("darkSwitch2","dark")
    }else{
        document.body.removeAttribute("data-theme");
        localStorage.removeItem("darkSwitch2")
    }
}