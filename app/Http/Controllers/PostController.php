<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
	use UploadFile;

	public function showPhotos($nickname, $id = null)
	{
		// dd($nickname, $id);
		// dd('test');
		$profile = User::whereNickname($nickname)->first();

		if (isset($profile)) {
			$posts = $profile->posts()->photo()->latest()->get();
			// dd(URL::full());
			// dd($id);
			$idpost = explode("#", $id)[0];
			return view('post', compact('posts', 'profile', 'idpost'));
		} else {
			return view('post');
		}
	}

	public function showVideos($nickname, $id = null)
	{
		// dd('test');
		$profile = User::whereNickname($nickname)->first();
		if (isset($profile)) {
			$posts = $profile->posts()->video()->latest()->get();
			$idpost = explode("#", $id)[0];
			return view('post', compact('posts', 'profile', 'idpost'));
		} else {
			return view('post');
		}
	}

	public function store(Request $request)
	{
		$request->validate([
			'thumbnail' => ['required', 'file', 'mimetypes:image/png,image/jpeg,image/svg+xml,video/mp4', 'max:10240'],
			// 'desc'      => 'required',
			// max:102400
		]);
		// dd($request);

		$result = $this->_uploadFile($request->thumbnail, 'img/posts');

		$post = new Post();
		$post->fill($request->input());

		if ($result['path']) {
			$post->thumbnail = $result['filename'];
		}

		// $post->fill($request->except(['_token', 'thumbnail']));


		$post->user_id  = Auth::id();
		// $post->desc     = $request->desc;
		// $post->is_video = $request->is_video;

		$post->save();

		Alert::success('Succeed!', 'New Post Added Successfully!');
		return back();
	}

	public function update(Request $request, Post $post)
	{
		$request->validate([
			'desc' => 'required',
		]);

		$post->desc = $request->desc;

		$post->save();

		Alert::success('Succeed!', 'Post Details Changed Successfully!');
		return back();
	}

	public function destroy(Post $post)
	{
		$post->delete();
		// dd(url()->previous());
		// Alert::success('Succeed!', 'Posting Succeed Dihapus!');
		// if (User::whereNickname($nickname)->posts()->photo()->get()) {
		return back();
		// } else {
		// return redirect()->route("profile", $nickname);
		// }
	}
}
