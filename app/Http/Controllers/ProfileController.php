<?php

namespace App\Http\Controllers;

use App\Models\Highlight;
use App\Models\Post;
use App\Models\User;
use App\Traits\UploadFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
	use UploadFile;

	public function index($nickname)
	{
		// dd($nickname);
		$profile    = User::whereNickname($nickname)->first();
		// $highlights = Highlight::whereUserId($profile->id)->get();
		// $posts      = Post::whereUserId($profile->id)->get();

		if (isset($profile)) {
			foreach ($profile->highlights as $highlight) {
				$diff = $highlight->created_at->diffInHours(Carbon::now());
				if ($diff >= 24) {
					$highlight->delete();
				}
				// if($highlight->id == 3) {
				//     dd($diff, $highlight->created_at, Carbon::now());
				// }
			}
			$highlights = $profile->highlights()->latest()->get();
			$photos      = $profile->posts()->photo()->latest()->get();
			$videos     = $profile->posts()->video()->latest()->get();
			// dd($highlights);

			return view('profile', compact('profile', 'photos', 'videos', 'highlights'));
		} else {
			return view('profile');
		}
	}

	public function update(Request $request, User $user)
	{
		$rules = [
			'name'     		=> 'required|max:30',
			'job'      		=> 'required',
			'nickname' 		=> 'required|unique:users,nickname,' . Auth::id(),
			'bio'			=> 'max:150',
			// 'email'    => 'required|email',
			// 'password' => 'required|confirmed',
			'photo'			=> 'image|max:4096',
			'background'    => 'image|max:4096',
		];

		// if (Auth::user()->bio) {
		// 	$rules['bio'] = 'required|max:80';
		// }

		$request->validate($rules);
		// dd($request->name);
		$user->fill($request->input());

		if ($request->photo) {
			$result_photo = $this->_uploadFile($request->photo, 'img/profiles');
			if ($result_photo["path"]) {
				$user->photo = $result_photo["filename"];
			}
		}
		if ($request->background) {
			$result_bg = $this->_uploadFile($request->background, 'img/profiles/bg');
			if ($result_bg["path"]) {
				$user->background = $result_bg["filename"];
			}
		}

		$user->save();
		Alert::success('Berhasil!', 'Detail Profile Berhasil Diubah!');
		return redirect()->route("profile", $user->nickname);
	}
}
