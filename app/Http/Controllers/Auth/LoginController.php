<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;
	// protected $redirectTo = route("profile", Auth::user()->nickname);

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	public function login(Request $request)
	{
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if (
			method_exists($this, 'hasTooManyLoginAttempts') &&
			$this->hasTooManyLoginAttempts($request)
		) {
			$this->fireLockoutEvent($request);

			return $this->sendLockoutResponse($request);
		}

		if ($this->attemptLogin($request)) {
			if ($request->hasSession()) {
				$request->session()->put('auth.password_confirmed_at', time());
			}
			$this->redirectTo = route('profile', Auth::user()->nickname);
			return $this->sendLoginResponse($request);
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	//tambahkan script di bawah ini
	public function redirectToProvider()
	{
		return Socialite::driver('google')->redirect();
	}


	//tambahkan script di bawah ini 
	public function handleProviderCallback(Request $request)
	{
		try {
			$user_google    = Socialite::driver('google')->user();
			$user           = User::where('email', $user_google->getEmail())->first();

			// jika user ada maka langsung di redirect ke halaman home
			// jika user tidak ada maka simpan ke database
			// $user_google menyimpan data google account seperti email, foto, dsb

			if ($user) {
				// dd($user);
				Auth::login($user);
				return redirect()->route('profile', Auth::user()->nickname);
			} else {
				$create = User::Create([
					'email'             => $user_google->getEmail(),
					'name'              => $user_google->getName(),
					'password'          => bcrypt('password'),
					'created_at' 		=> Carbon::now(),
					'updated_at' 		=> Carbon::now(),
					'email_verified_at' => Carbon::now()
				]);
				$create->nickname = "User" . $create->id;
				$create->save();

				// dd($create->nickname);
				// auth()->login($create, true);
				Auth::login($create);
				return redirect()->route('profile', Auth::user()->nickname);
			}
		} catch (Exception $e) {
			return redirect()->route('login');
		}
	}
}
