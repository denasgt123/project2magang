<?php

namespace App\Http\Controllers;

use App\Models\Highlight;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class HighlightController extends Controller
{
	use UploadFile;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			// 'title'     => 'required',
			// 'thumbnail' => 'required|file|mimes:jpg,png,svg,mp4|max:51200',
			// 'thumbnail' => ['required', 'file', 'mimetypes:image/png,image/jpeg,image/svg+xml,video/mp4', 'max:10240'],
			'thumbnail' => ['required', 'file', 'mimetypes:image/png,image/jpeg,image/svg+xml,video/mp4', 'max:10240'],
			// max:5120
			// 'desc'      => 'required',
		]);
		// dd($request);

		$result = $this->_uploadFile($request->thumbnail, 'img/highlights');

		$highlight = new Highlight();
		$highlight->fill($request->input());

		if ($result['path']) {
			$highlight->thumbnail = $result['filename'];
		}

		// $highlight->fill($request->except(['_token', 'thumbnail']));

		$highlight->user_id  = Auth::id();
		// $highlight->title    = $request->title;
		$highlight->is_video = $request->file('thumbnail')->extension() == 'mp4';

		$highlight->save();

		Alert::success('Succeed!', 'New Highlights Added Successfully!');
		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Highlight $highlight)
	{
		$request->validate([
			// 'title' => 'required',
			'desc' => 'required',
		]);

		$highlight->fill($request->input());

		// $highlight->desc = $request->desc;
		// $highlight->title = $request->title;

		$highlight->save();

		Alert::success('Succeed!', 'Highlight Details Changed Successfully!');
		return back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Highlight $highlight)
	{
		$highlight->delete();
		// Alert::success('Succeed!', 'Highlight Telah Dihapus!');
		return back();
	}
}
