@extends('layout.app')

@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
    <div class="container min-vh-100 d-flex align-items-center justify-content-center">
        <div class="row d-flex justify-content-center w-100">
            <div class="col-xl-7 col-lg-6 col-md-4 col-12 px-5 d-none d-lg-block ">
                <img class="w-100 my-5 pe-5" src="{{ asset('img/actur besar.png') }}" alt="Logo Acture">
                <h4 class="fw-normal">Aplikasi untuk profile karyawan Burningroom dengan tampilan sosial media.</h4>
            </div>
            <div class="col-12 col-md-8 col-lg-6 col-xl-5 align-self-center">
                <div class="card login-box" style="border-radius: 1rem;">
                    <div class="card-body px-5">
                        <h2 class="my-4 fw-bold">Sign in</h2>
                        <div class="form-outline mb-4">
                            <label class="form-label" for="typeEmailX-2">Email</label>
                            <input type="email" id="typeEmailX-2" class="form-control form-control-md" autofocus />
                        </div>

                        <div class="form-outline mb-4">
                            <div class="row justify-content-between">
                                <div class="col">
                                    <label class="form-label" for="typePasswordX-2">Password</label>
                                </div>
                                <div class="col text-end">
                                    <a href="#">Forgot Password</a>
                                </div>
                            </div>

                            <input type="password" id="typePasswordX-2" class="form-control form-control-md" />
                        </div>

                        <!-- Checkbox -->
                        <div class="row justify-content-between">
                            <div class="col-12 col-sm-6 py-2 py-sm-0 d-flex align-items-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="form1Example3" />
                                    <label class="form-check-label px-2" for="form1Example3">Remember password</label>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 py-2 py-sm-0 text-sm-end text-center">
                                <button class="btn btn-primary btn-md px-sm-4 px-5 rounded-pill"
                                    type="submit">Login</button>
                            </div>
                        </div>

                        <p class="text-center my-4">Not a member? <a data-toggle="tab" href="#signup">Sign Up</a></p>
                    </div>
                </div>

                <div class="row mt-5 mb-4">
                    <div class="col-5 gx-0">
                        <hr>
                    </div>
                    <div class="col-2 gx-0">
                        <p class="text-muted text-center">Or</p>
                    </div>
                    <div class="col-5 gx-0">
                        <hr>
                    </div>
                </div>

                <button class="btn btn-lg text-white w-100 google-btn" style="background-color: #dd4b39;" type="submit"><i
                        class="fab fa-google"></i> Sign in with google</button>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
