{{-- PWA Script Service Worker --}}
<script>
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register("/sw2.js").then(function(reg) {
            console.log("Service worker has been registered for scope: " + reg.scope);
            console.log('test');
        });
    }
    console.log('test2');
</script>

{{-- Jquerry for Bootstrap CDN --}}
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>

{{-- Bootstrap 5.2 Javascript CDN --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
</script>

{{-- Box Icons for Sosmed --}}
{{-- <script src="https://unpkg.com/boxicons@2.1.2/dist/boxicons.js"></script> --}}

{{-- AOS Script CDN --}}
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

{{-- Swal Script CDN --}}
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

{{-- Dark Mode --}}
<script>
    let currentRouteName = "{{ Route::currentRouteName() }}";
    if (localStorage.getItem("event") !== null) {
        console.log(localStorage.getItem("event"));
        localStorage.removeItem("event");
    }

    if (localStorage.getItem("response") !== null) {
        console.log("response :" + localStorage.getItem("response"));
        localStorage.removeItem("response");
    }
</script>
<script src="{{ asset('js/dark-mode-switch.js') }}"></script>
