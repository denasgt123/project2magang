<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('img/logo.png') }}">
    <title>Acttur Application</title>

    {{-- Style for All Page --}}
    @include('layout.style')

    {{-- Specific Style Page --}}
    @yield('style')
</head>

<body class="min-vh-100 h-auto bg-main">
    {{-- Page Content --}}
    @yield('modal')

    @yield('content')

    @include('sweetalert::alert', ['cdn' => 'https://cdn.jsdelivr.net/npm/sweetalert2@9'])

    {{-- Script for All Page --}}
    @include('layout.script')

    {{-- Specific Script Page --}}
    @yield('script')
</body>

</html>
