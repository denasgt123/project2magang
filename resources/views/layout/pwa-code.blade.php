<!-- PWA  -->
<meta name="theme-color" content="#6777ef" />
<link rel="apple-touch-icon" href="{{ asset('img/logo.png') }}">
<link rel="manifest" href="{{ asset('/manifest.json') }}">

<!-- PWA Chrome Android  -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="ACT">
<link rel="icon" sizes="512x512" href="{{ asset('img/logo.png') }}">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-ba+r-style" content="black">
<meta name="apple-mobile-web-app-title" content="ACT">

{{-- PWA Script Service Worker --}}
<script>
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register("/sw.js").then(function(reg) {
            console.log("Service worker has been registered for scope: " + reg.scope);
            console.log('test');
        });
    }
    console.log('test2');
</script>