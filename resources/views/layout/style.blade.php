{{-- Bootstrap 5.2 Style CSS CDN --}}
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

{{-- Font Awesome kit CDN --}}
<script src="https://kit.fontawesome.com/80a5c9094f.js" crossorigin="anonymous"></script>

{{-- AOS Animation CDN --}}
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

<!-- PWA  -->
<meta name="theme-color" content="#6777ef" />
<link rel="apple-touch-icon" href="{{ asset('img/logo.png') }}">
<link rel="manifest" href="{{ asset('/manifest.json') }}">

<!-- PWA Chrome Android  -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="ACT">
<link rel="icon" sizes="512x512" href="{{ asset('img/logo.png') }}">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-ba+r-style" content="black">
<meta name="apple-mobile-web-app-title" content="ACT">

{{-- Style All Pages --}}
<link rel="stylesheet" href="{{ asset('css/style.css') }}">

{{-- Dark Mode --}}
<link rel="stylesheet" href="{{ asset('css/dark-mode.css') }}">
