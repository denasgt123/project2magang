@extends('layout.app')

@section('style')
    {{-- PWA Code
    @include('layout.pwa-code') --}}

    {{-- CSS Style for Profile Page --}}
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection

@section('modal')
    @if (isset($profile))
        @auth
            {{-- EDIT PROFILE START --}}
            <div class="modal fade" id="editProfile" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <form action="{{ route('profile.update', Auth::id()) }}" method="post" id="editProfile-form"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="modal-header" style="border-bottom: none;">
                                <h5 class="modal-title" id="staticBackdropLabel">Edit Profile</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row h-100">
                                    <div class="col-12">
                                        <div class="header-section w-100 position-relative neumorph-light overflow-hidden">
                                            <img class="position-absolute top-50 start-50 translate-middle w-100 h-100"
                                                src="{{ asset("img/profiles/bg/$profile->background") }}" alt="Background Img"
                                                style="object-fit: cover;">
                                            <div
                                                class="bg-overlay w-100 h-100 position-absolute top-50 start-50 translate-middle">
                                            </div>
                                            <div
                                                class="header-box position-absolute top-50 start-50 translate-middle d-flex justify-content-center align-items-center">
                                                <div class="d-flex flex-wrap w-100 h-100" data-aos="fade-down"
                                                    data-aos-duration="700">
                                                    <div
                                                        class="col-lg-3 col-md-4 col-12 d-flex justify-content-center align-items-center align-self-center">
                                                        <div
                                                            class="prev-prof-pic-container overflow-hidden d-flex justify-content-center align-items-center">
                                                            <img class="prof-pic w-100 h-100 profile-img-preview"
                                                                src="{{ asset("img/profiles/$profile->photo") }}" alt="Denas"
                                                                style="object-fit: cover;">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-8 col-12 d-flex align-items-center">
                                                        <div class="px-xl-5 px-lg-4 px-3">
                                                            <h4
                                                                class="text-white mb-md-2 mb-0 text-center text-md-start name-preview fs-4">
                                                                {{ $profile->name }}</h4>
                                                            <p
                                                                class="text-light fw-semibold fs-6 mb-md-2 mb-0 text-center text-md-start job-preview">
                                                                {{ $profile->job }}</p>
                                                            <p
                                                                class="text-break text-light fw-light fs-6 mt-md-3 mt-0 mb-0 text-center text-md-start bio-preview">
                                                                {{ $profile->bio }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-4">
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeNameX-2">Name</label>
                                            <input name="name" type="text" id="typeNameX-2"
                                                class="form-control form-control-md" placeholder="My Full Name"
                                                value="{{ $profile->name }}" maxlength="30" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeUsernameX-2">Username</label>
                                            <input name="nickname" type="text" id="typeUsernameX-2"
                                                class="form-control form-control-md" placeholder="My Username"
                                                value="{{ $profile->nickname }}" maxlength="30" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeJobX-2">Job</label>
                                            <input name="job" type="text" id="typeJobX-2"
                                                class="form-control form-control-md" placeholder="My Job"
                                                value="{{ $profile->job }}" maxlength="50" />
                                            {{-- <select name="job" id="typeJobX-2" class="form-select form-select-md">
                                            <option value="Junior Programmer"
                                                {{ $profile->job == 'Junior Programmer' ? 'selected' : '' }}>Junior Programmer
                                            </option>
                                            <option value="Senior Programmer"
                                                {{ $profile->job == 'Senior Programmer' ? 'selected' : '' }}>Senior Programmer
                                            </option>
                                            <option value="Marketing" {{ $profile->job == 'Marketing' ? 'selected' : '' }}>
                                                Marketing</option>
                                            <option value="Manager" {{ $profile->job == 'Manager' ? 'selected' : '' }}>
                                                Manager</option>
                                            <option value="UI/UX Desainer"
                                                {{ $profile->job == 'UI/UX Desainer' ? 'selected' : '' }}>UI/UX Desainer
                                            </option>
                                        </select> --}}
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeBioX-2">Bio</label>
                                            <textarea name="bio" class="form-control form-control-md" id="typeBioX-2" rows="10" placeholder="My Bio"
                                                maxlength="120">{{ $profile->bio }}</textarea>
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeEditProfileImageX-2">Input Profile
                                                Image</label>
                                            <input name="photo" type="file" id="typeEditProfileImageX-2"
                                                class="form-control form-control-md" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeEditBackgroundImageX-2">Input Background
                                                Image</label>
                                            <input name="background" type="file" id="typeEditBackgroundImageX-2"
                                                class="form-control form-control-md" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="border-top: none;">
                                <button type="submit" class="btn btn-success px-4" form="editProfile-form"
                                    id="editProfile-form-btn">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- EDIT PROFILE END --}}

            {{-- ADD POST - PHOTO START --}}
            <div class="modal fade" id="addPhoto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <form action="{{ route('posts.store') }}" method="post" id="addPhoto-form"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="is_video" value="0">
                            <div class="modal-header" style="border-bottom: none;">
                                <h5 class="modal-title" id="staticBackdropLabel">Upload New Post Photo</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row h-100">
                                    <div class="col-lg-7 col-12 mb-3 mb-lg-0">
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center border border-1 border-secondary rounded preview-placeholder">
                                            <div class="text-center">
                                                <i class="fs-1 text-muted fa-regular fa-image"></i>
                                                <h6 class="text-muted pt-2">Image Preview</h6>
                                            </div>
                                        </div>
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview">
                                            <img class="w-100 h-100" src=".." alt="Post1"
                                                style="max-height: 40rem; object-fit: contain">
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-12">
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typePostImageX-2">Input
                                                Image <span class="text-danger fw-bold">*</span></label>
                                            <input name="thumbnail" type="file" id="typePostImageX-2"
                                                class="form-control form-control-md" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeDescriptionX-2">Description</label>
                                            <textarea name="desc" class="form-control form-control-md pb-0" id="typeDescriptionX-2" rows="10"
                                                placeholder="Type a description for new post" maxlength="800"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="border-top: none;">
                                <button type="submit" class="btn btn-success px-4" id="addPhoto-form-btn">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- ADD POST - PHOTO END --}}

            {{-- ADD POST - VIDEO START --}}
            <div class="modal fade" id="addVideo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <form action="{{ route('posts.store') }}" method="post" id="addVideo-form"
                            enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="is_video" value="1">
                            <div class="modal-header" style="border-bottom: none;">
                                <h5 class="modal-title" id="staticBackdropLabel">Upload New Post Video</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row h-100">
                                    <div class="col-lg-7 col-12 mb-3 mb-lg-0">
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center border border-1 border-secondary rounded preview-placeholder">
                                            <div class="text-center">
                                                <i class="fs-1 text-muted fa-solid fa-video"></i>
                                                <h6 class="text-muted pt-2">Video Preview</h6>
                                            </div>
                                        </div>
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview">
                                            <video class="w-100 h-100" style="max-height: 40rem; object-fit: contain"
                                                controls>
                                                <source src="..">
                                                Your browser does not support HTML video.
                                            </video>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-12">

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typePostVideoX-2">Input
                                                Video <span class="text-danger fw-bold">*</span></label>
                                            <input name="thumbnail" type="file" id="typePostVideoX-2"
                                                class="form-control form-control-md" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeDescriptionX-2">Description</label>
                                            <textarea name="desc" class="form-control form-control-md pb-0" id="typeDescriptionX-2" rows="10"
                                                placeholder="Type a description for new post" maxlength="800"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="border-top: none;">
                                <button type="submit" class="btn btn-success px-4" id="addVideo-form-btn">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- ADD POST - VIDEO END --}}

            {{-- QRCODE START --}}
            <div class="modal fade" id="Qrcode" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: none;">
                            <h5 class="modal-title" id="staticBackdropLabel">QrCode</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body text-center p-5 modal-qrcode">
                            {!! QrCode::generate(Request::url()) !!}
                        </div>
                        <div class="modal-footer justify-content-center pb-lg-4 pb-3" style="border-top: none;">
                            <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(500)->generate(Request::url())) }}"
                                class="btn btn-success" download="qrcode">Download</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- QRCODE END --}}

            {{-- ADD HIGHLIGHT START --}}
            <div class="modal fade" id="addHighlight" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <form action="{{ route('highlights.store') }}" method="post" id="addHighlight-form"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header" style="border-bottom: none;">
                                <h5 class="modal-title" id="staticBackdropLabel">Add Highlight</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row h-100">
                                    <div class="col-lg-7 col-12 mb-3 mb-lg-0">
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center border border-1 border-secondary rounded preview-placeholder">
                                            <div class="text-center">
                                                <i class="fs-1 text-muted fa-solid fa-file-arrow-down"></i>
                                                {{-- <i class="fs-1 text-muted fa-regular fa-image"></i> --}}
                                                <h6 class="text-muted pt-2">File Preview</h6>
                                            </div>
                                        </div>
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview-img">
                                            <img class="w-100 h-100" src=".." alt="Post1"
                                                style="max-height: 40rem; object-fit: contain">
                                        </div>
                                        <div
                                            class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview-vid">
                                            <video class="w-100 h-100" style="max-height: 40rem; object-fit: contain"
                                                controls>
                                                <source src="..">
                                                Your browser does not support HTML video.
                                            </video>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-12">
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeHighlightFileX-2">Input
                                                File <span class="text-danger fw-bold">*</span></label>
                                            <input name="thumbnail" type="file" id="typeHighlightFileX-2"
                                                class="form-control form-control-md" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeTitleX-2">Title</label>
                                            <input name="title" type="text" id="typeTitleX-2"
                                                class="form-control form-control-md" placeholder="My Moment"
                                                maxlength="15" />
                                        </div>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="typeDescriptionX-2">Description</label>
                                            <textarea name="desc" class="form-control form-control-md pb-0" id="typeDescriptionX-2" rows="7"
                                                placeholder="Type a description for Highlight Post" maxlength="800"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="border-top: none;">
                                <button type="submit" class="btn btn-success px-4" id="addHighlight-form-btn">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- ADD HIGHLIGHT END --}}

            {{-- EDIT HIGHLIGHT START --}}
            <div class="modal fade" id="editHighlight" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <form action="" method="post" id="edit-form-modal">
                            @csrf
                            @method('PUT')
                            <div class="modal-header" style="border-bottom: none;">
                                <h5 class="modal-title" id="staticBackdropLabel">Edit Highlight
                                </h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="row h-100">
                                    <div class="form-outline mb-4">
                                        <label class="form-label" for="edit-highlight-title">Title</label>
                                        <input name="title" type="text" id="edit-highlight-title"
                                            class="form-control form-control-md" placeholder="My Moment" maxlength="15" />
                                    </div>
                                    <div class="form-outline mb-4">
                                        <label class="form-label" for="edit-highlight-desc">Description <span
                                                class="text-danger fw-bold">*</span></label>
                                        <textarea name="desc" class="form-control form-control-md pb-0" id="edit-highlight-desc" rows="10"
                                            placeholder="Type a description for this post" maxlength="800"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="border-top: none;">
                                <button type="submit" class="btn btn-success px-4">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- EDIT HIGHLIGHT START --}}
        @endauth

        {{-- HIGHLIGHT ITEM START --}}
        <div class="modal fade" id="highlightItem" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header pb-0" style="border-bottom: none;">
                        <div class="d-flex flex-wrap w-100 justify-content-between">
                            <div class="col-11 d-flex">
                                <img class="highlight-prof-pic overflow-hidden rounded-circle" src=""
                                    alt="Denas" style="object-fit: cover;">
                                <div class="ms-3">
                                    <h5 class="pb-0 mb-0 highlight-user-name"></h5>
                                    <p class="pt-0 mt-0 fw-light highlight-user-time"></p>
                                </div>
                            </div>
                            <div class="col-1 d-flex align-items-start justify-content-end">
                                @auth
                                    @if (Auth::id() == $profile->id)
                                        <div class="btn-group dropstart">
                                            <button type="button" data-bs-toggle="dropdown" aria-expanded="false"
                                                class="border-0 bg-transparent highlight-option">
                                                <i class="fa-solid fa-ellipsis-vertical"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a class="dropdown-item edit-highlight-btn text-purple"
                                                        href="javascript:void(0)" data-bs-toggle="modal"
                                                        data-bs-target="#editHighlight" data-title="" data-desc="">
                                                        <i class="fa-solid fa-pen pe-2"></i> Edit this Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item delete-highlight-btn text-purple"
                                                        href="javascript:void(0)">
                                                        <i class="fa-solid fa-trash pe-2"></i> Delete this Post
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                @endauth
                            </div>
                            {{-- <div class="col-1 d-flex align-items-start justify-content-end">
                            <a href="javascript:void(0)" class="text-dark text-decoration-none fs-5"
                                data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></a>
                        </div> --}}
                        </div>
                    </div>
                    <div class="modal-body p-0">
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <img class="w-100 h-100 highlight-file-img d-none" src="" alt="Highlight"
                                style="max-height: 40rem; object-fit: contain">
                            <video class="w-100 h-100 highlight-file-video d-none"
                                style="max-height: 40rem; object-fit: contain" controls>
                                <source src="">
                                Your browser does not support HTML video.
                            </video>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-start" style="border-top: none;">
                        <p class="highlight-description"></p>
                    </div>
                </div>
            </div>
        </div>
        {{-- HIGHLIGHT ITEM END --}}

        {{-- PREVIEW IMAGE START --}}
        <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content overflow-hidden">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0 d-flex justify-content-center align-items-center">
                        <img src="" alt="" class="w-100 h-100" style="object-fit: contain">
                    </div>
                </div>
            </div>
        </div>
        {{-- PREVIEW IMAGE END --}}

        <form action="" method="POST" id="delete-form">
            @csrf
            @method('DELETE')
        </form>
    @endif
@endsection

@section('content')
    @if (isset($profile))
        {{-- Navbar Start --}}
        <nav class="navbar navbar-expand-md fixed-top bg-light d-none d-lg-block px-4">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img class="img-logo" src="{{ asset('img/actur besar.png') }}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        @auth
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">New Posting</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="dropdown-item" href="#" data-bs-toggle="modal"
                                            data-bs-target="#addPhoto"><i class="fa-solid fa-image pe-2"></i> Upload Photo</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#" data-bs-toggle="modal"
                                            data-bs-target="#addVideo"><i class="fa-solid fa-video pe-2"></i> Upload Video</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#Qrcode"><i
                                        class="fa-solid fa-qrcode"></i> QrCode</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#editProfile">Edit
                                    Profile</a>
                            </li>
                        @endauth
                        <li class="nav-item">
                            <a class="nav-link darkSwitch" href="javascript:void(0)" id="darkSwitch-desktop">
                                <i class="fa-solid fa-moon"></i> Nightmode
                            </a>
                        </li>
                        @auth
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
        {{-- Navbar End --}}

        {{-- Content Website Start --}}
        <div class="container-xl h-100 p-0 mt-lg-5 mt-0">
            {{-- Header Start --}}
            <div class="header-section neumorph-light w-100 position-relative overflow-hidden">
                <img class="w-100 h-100 position-absolute top-50 start-50 translate-middle "
                    src="{{ asset("img/profiles/bg/$profile->background") }}" alt="Background Img"
                    style="object-fit: cover;">
                <div class="w-100 h-100 bg-overlay position-absolute top-50 start-50 translate-middle"></div>
                <div
                    class="header-box position-absolute top-50 start-50 translate-middle pb-3 pb-sm-0 d-flex justify-content-center align-items-center">
                    <div class="d-flex flex-wrap w-100 h-100" data-aos="fade-down" data-aos-duration="700">
                        <div
                            class="col-lg-3 col-sm-4 col-12 d-flex justify-content-center align-items-center align-self-center">
                            <div
                                class="prof-pic-container overflow-hidden d-flex justify-content-center align-items-center">
                                <a href="javascript::void(0)" data-bs-toggle="modal" data-bs-target="#imageModal"
                                    data-bs-image="{{ asset("img/profiles/$profile->photo") }}"
                                    class="prof-pic w-100 h-100">
                                    <img class="w-100 h-100" src="{{ asset("img/profiles/$profile->photo") }}"
                                        alt="Denas" style="object-fit: cover;">
                                </a>
                            </div>
                        </div>
                        <div
                            class="col-lg-9 col-sm-8 col-12 d-flex align-items-center justify-content-sm-start justify-content-center">
                            <div class="px-lg-5">
                                <h3 class="text-white mb-lg-2 mb-0 text-sm-start text-center mt-2 mt-sm-0 profile-name">
                                    {{ $profile->name }}</h3>
                                <p
                                    class="text-light fw-semibold fst-italic mb-md-2 mb-0 text-sm-start text-center profile-job">
                                    {{ $profile->job }}</p>
                                <p
                                    class="text-break text-light fw-light mt-md-3 mt-0 mb-0 text-sm-start text-center profile-bio">
                                    {{ $profile->bio }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Header End --}}

            {{-- Highlight Start --}}
            @if (count($highlights) > 0)
                <div class="splide w-100 highlight-section">
                    <div class="splide__track ps-0 pe-5 overflow-hidden highlight-slider py-xxl-5 py-4">
                        <div class="splide__list px-lg-4 px-3 d-flex">
                            @auth
                                @if (Auth::user()->id == $profile->id)
                                    <div class="splide__slide px-3 d-flex justify-content-center align-items-start">
                                        <a href="#"
                                            class="highlight-item text-decoration-none d-flex flex-column align-items-center"
                                            data-bs-toggle="modal" data-bs-target="#addHighlight">
                                            <div
                                                class="highlight-add highlight-thumbnail d-flex align-items-center justify-content-center neumorph-light">
                                                <i class="fa-solid fa-plus fs-3 text-white"></i>
                                            </div>
                                            <p class="text-center fw-bold text-dark mb-0 lh-sm highlight-title">
                                                Add Highlight
                                            </p>
                                        </a>
                                    </div>
                                @endif
                            @endauth
                            @forelse ($highlights as $item)
                                <div class="splide__slide px-3 d-flex justify-content-center align-items-start">
                                    <a href="#"
                                        class="highlight-item text-decoration-none d-flex flex-column align-items-center"
                                        data-bs-toggle="modal" data-bs-target="#highlightItem"
                                        data-href-edit="{{ route('highlights.update', $item->id) }}"
                                        data-href-delete="{{ route('highlights.destroy', $item->id) }}"
                                        data-file="{{ asset("img/highlights/$item->thumbnail") }}"
                                        data-title="{{ $item->title }}"
                                        data-time="{{ $item->created_at->diffForHumans() }}"
                                        data-desc="{{ $item->desc }}" data-is-video="{{ $item->is_video }}"
                                        data-user-photo="{{ asset('img/profiles/' . $item->user->photo) }}"
                                        data-user-name="{{ $item->user->name }}" data-user-job="{{ $item->user->job }}">
                                        <div class="highlight-thumbnail neumorph-light overflow-hidden">
                                            @if ($item->is_video)
                                                <video class="w-100 h-100" style="max-height: 40rem; object-fit: cover"
                                                    nocontrols>
                                                    <source src="{{ asset("img/highlights/$item->thumbnail") }}">
                                                    Your browser does not support HTML video.
                                                </video>
                                            @else
                                                <img src="{{ asset("img/highlights/$item->thumbnail") }}" alt="highlight"
                                                    class="w-100 h-100" style="object-fit: cover;">
                                            @endif
                                        </div>
                                        <p class="text-center fw-bold text-dark mb-0 lh-sm highlight-title">
                                            {{ $item->title }}</p>
                                    </a>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            @else
                @auth
                    <div class="splide w-100 highlight-section">
                        <div class="splide__track ps-0 pe-5 overflow-hidden highlight-slider py-xl-5 py-lg-4 py-3">
                            <div class="splide__list px-4 d-flex">
                                @if (Auth::user()->id == $profile->id)
                                    <div class="splide__slide px-3 d-flex justify-content-center align-items-start">
                                        <a href="#"
                                            class="highlight-item text-decoration-none d-flex flex-column align-items-center"
                                            data-bs-toggle="modal" data-bs-target="#addHighlight">
                                            <div
                                                class="highlight-add highlight-thumbnail neumorph-light d-flex align-items-center justify-content-center">
                                                <i class="fa-solid fa-plus fs-3 text-white"></i>
                                            </div>
                                            <p class="text-center fw-bold text-dark mt-4 mb-0 lh-sm">Add Highlight</p>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="w-100 highlight-section d-flex justify-content-center align-items-center">
                        <h4 class="my-5 py-4">User ini Belum memiliki Highlight.</h4>
                    </div>
                @endauth
            @endif
            {{-- Highlight End --}}

            {{-- User Post Start --}}
            <div class="post-section neumorph-light w-100">
                {{-- Post Button Toggle Start --}}
                <div class="post-options d-flex">
                    <a href="javascript:void(0)"
                        class="text-decoration-none col-6 active d-flex justify-content-center align-items-center post-option-photo">
                        <i class="fa-solid fa-grip fs-lg-1 fs-md-2 fs-sm-3 fs-2"></i>
                    </a>
                    <a href="javascript:void(0)"
                        class="text-decoration-none col-6 d-flex justify-content-center align-items-center post-option-video">
                        <i class="fa-solid fa-play fs-lg-2 fs-md-3 fs-sm-4 fs-4"></i>
                    </a>
                </div>
                {{-- Post Button Toggle End --}}

                {{-- Post Content Start --}}
                <div class="post-items w-100 p-md-1 py-1 px-0">

                    {{-- Post Photo Start --}}
                    <div class="row g-lg-2 g-1 post-item-photos active" style="height: 0; opacity: 0; overflow: hidden"
                        data-aos="fade-in" data-aos-duration="1500">
                        @forelse ($photos as $item)
                            @if (!$item->is_video)
                                <a href="{{ route('photos', ['nickname' => $item->user->nickname, 'id' => 'post' . $item->id]) }}"
                                    class="col-lg-2 col-md-3 col-4">
                                    <div class="position-relative w-100" style="padding-top: 100%;">
                                        <div class="w-100 h-100 position-absolute top-50 start-50 translate-middle">
                                            <img class="w-100 h-100" src="{{ asset("img/posts/$item->thumbnail") }}"
                                                alt="Photo" style="object-fit: cover">
                                        </div>
                                    </div>
                                </a>
                            @endif
                        @empty
                            <div class="d-flex w-100 h-100 justify-content-center align-items-center my-5 py-5">
                                <h5>User ini Belum memiliki Postingan.</h5>
                            </div>
                        @endforelse
                    </div>
                    {{-- Post Photo End --}}

                    {{-- Post Video Start --}}
                    <div class="row g-lg-2 g-1 post-item-videos py-2" style="height: 0; opacity: 0; overflow: hidden"
                        data-aos="fade-in" data-aos-duration="1500">
                        {{-- Example Post Real Video --}}
                        @forelse ($videos as $item)
                            <div class="col-4">
                                <div class="position-relative w-100" style="padding-top: 100%;">
                                    <div class="w-100 h-100 position-absolute top-50 start-50 translate-middle">
                                        <a href="{{ route('videos', ['nickname' => $item->user->nickname, 'id' => 'post' . $item->id]) }} "
                                            class="text-decoration-none">
                                            <video class="w-100 h-100" style="object-fit: cover" nocontrols>
                                                <source src="{{ asset("img/posts/$item->thumbnail") }}">
                                                Your browser does not support HTML video.
                                            </video>
                                            <div
                                                class="w-100 h-100 position-absolute top-50 start-50 translate-middle bg-video">
                                            </div>
                                            <i
                                                class="position-absolute top-50 start-50 translate-middle fa-solid fa-play display-1 text-white play-icon 1"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="d-flex w-100 h-100 justify-content-center align-items-center my-5 py-5">
                                <h5>User ini Belum memiliki Video.</h5>
                            </div>
                        @endforelse
                    </div>
                    {{-- Post Video End --}}

                </div>
                {{-- Post Content End --}}

                {{-- Footer Start --}}
                <div class="footer text-center py-4 d-none d-md-block">
                    <h6>Copyright {{ date('Y') }}, All rights reserved</h6>
                </div>
                {{-- Footer End --}}

            </div>
            {{-- User Post End --}}

            {{-- Navbar for Mobile Start --}}
            <div class="navbar-mobile-section neumorph-light w-100 fixed-bottom d-block d-lg-none bg-main">
                <div class="h-100 row px-4">
                    @auth
                        <div class="col-3 d-flex justify-content-center dropup">
                            <a href=""
                                class="d-flex flex-column pt-3 justify-content-center text-decoration-none text-dark"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fs-3 fa-regular fa-square-plus align-self-center pb-1"></i>
                                <p class="navbar-mobile-text mb-2 text-center">New Post</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="#" data-bs-toggle="modal"
                                        data-bs-target="#addPhoto"><i class="fa-solid fa-image pe-2"></i> Upload Photo</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="#" data-bs-toggle="modal"
                                        data-bs-target="#addVideo"><i class="fa-solid fa-video pe-2"></i> Upload Video</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3 d-flex justify-content-center">
                            <a href=""
                                class="d-flex flex-column pt-3 justify-content-center text-decoration-none text-dark"
                                data-bs-toggle="modal" data-bs-target="#Qrcode">
                                <i class="fs-3 fa-solid fa-qrcode align-self-center pb-1"></i>
                                <p class="navbar-mobile-text mb-2 text-center">Qr Code</p>
                            </a>
                        </div>
                        <div class="col-3 d-flex justify-content-center">
                            <a href="javascript:void(0)"
                                class="d-flex flex-column pt-3 justify-content-center text-decoration-none text-dark"
                                id="darkSwitch-mobile">
                                {{-- <i class="fs-3 fa-solid fa-sun"></i> --}}
                                <i class="fs-3 fa-solid fa-moon align-self-center pb-1"></i>
                                <p class="navbar-mobile-text mb-2 text-center">Night Mode</p>
                            </a>
                        </div>
                        <div class="col-3 d-flex justify-content-center dropup">
                            <a href=""
                                class="d-flex flex-column pt-3 justify-content-center text-decoration-none text-dark"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fs-3 fa-solid fa-user align-self-center pb-1"></i>
                                <p class="navbar-mobile-text mb-2 text-center">User</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="#" data-bs-toggle="modal"
                                        data-bs-target="#editProfile"><i class="fa-solid fa-user-pen pe-2"></i> Edit
                                        Profile</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa-solid fa-right-from-bracket pe-2"></i> Logout
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @else
                        <div class="col-6 d-flex justify-content-center">
                            <a href="javascript:void(0)"
                                class="d-flex flex-column pt-3 justify-content-center text-decoration-none text-dark"
                                id="darkSwitch-mobile">
                                {{-- <i class="fs-3 fa-solid fa-sun"></i> --}}
                                <i class="fs-3 fa-solid fa-moon align-self-center pb-1"></i>
                                <p class="navbar-mobile-text mb-2 text-center">Night Mode</p>
                            </a>
                        </div>
                        <div class="col-6 d-flex justify-content-center dropup">
                            <a href=""
                                class="d-flex flex-column pt-3 justify-content-center text-decoration-none text-dark"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fs-3 fa-solid fa-user align-self-center pb-1"></i>
                                <p class="navbar-mobile-text mb-2 text-center">User</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="{{ route('login') }}"><i
                                            class="fa-solid fa-right-to-bracket pe-2"></i> Login</a>
                                </li>
                            </ul>
                        </div>
                    @endauth
                </div>
            </div>
            {{-- Navbar for Mobile End --}}
        </div>
        {{-- Content Website End --}}
    @else
        <div class="container min-vh-100 d-flex justify-content-center align-items-center">
            <h2 class="text-center">Sorry, User not found !!</h2>
        </div>
    @endif
@endsection

@section('script')
    {{-- Splide JS CDN --}}
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.2/dist/js/splide.min.js"></script>

    {{-- Script for this Page --}}
    <script>
        $(document).ready(function() {
            //     Swal.fire(
            //         'Error!',
            //         '$error',
            //         'error'
            //     )

            // Get current route name for darkmode 
            let currentRouteName = "{{ Route::currentRouteName() }}";
            // console.log(currentRouteName);

            // AOS Animation Start
            AOS.init();
            // AOS Animation End

            // Start script click post type animation
            $(".post-option-photo").on("click", function(event) {
                // event.preventDefault();
                // console.log('wadidaw1');
                $(".post-option-photo").addClass("active");
                $(".post-item-photos").addClass("active");
                $(".post-option-video").removeClass("active");
                $(".post-item-videos").removeClass("active");
            });

            $(".post-option-video").on("click", function(event) {
                // event.preventDefault();
                // console.log('wadidaw2');
                $(".post-option-video").addClass("active");
                $(".post-item-videos").addClass("active");
                $(".post-option-photo").removeClass("active");
                $(".post-item-photos").removeClass("active");
            });
            // End script click post type animation

            // Start Script Video Post Hover Animation
            $(".bg-video").hover(function(event) {
                event.preventDefault();
                // console.log($(this).prev());
                $(this).next().css("opacity", "1");
                $(this).css("background-color", "#000000");
                $(this).css("opacity", "0.2");
            }, function(event) {
                event.preventDefault();
                // $(this).prev().css("background-color", "#000000");
                $(this).next().css("opacity", "0.7");
                $(this).css("opacity", "0");
            });

            $(".play-icon").hover(function(event) {
                event.preventDefault();
                // console.log($(this).prev());
                $(this).css("opacity", "1");
                $(this).prev().css("background-color", "#000000");
                $(this).prev().css("opacity", "0.2");
            }, function(event) {
                event.preventDefault();
                // $(this).prev().css("background-color", "#000000");
                $(this).css("opacity", "0.7");
                $(this).prev().css("opacity", "0");
            });
            // End Script Video Post Hover Animation

            // Start Script View Highlight Item
            $('#highlightItem').on('show.bs.modal', event => {
                // Button that triggered the modal
                const button = event.relatedTarget;

                // Data in Triggered Button
                const edit_href = button.getAttribute('data-href-edit');
                const delete_href = button.getAttribute('data-href-delete');
                const file = button.getAttribute('data-file');
                const title = button.getAttribute('data-title');
                const time = button.getAttribute('data-time');
                const desc = button.getAttribute('data-desc');
                const is_video = button.getAttribute('data-is-video');
                const user_photo = button.getAttribute('data-user-photo');
                const user_name = button.getAttribute('data-user-name');
                const user_job = button.getAttribute('data-user-job');

                // Update the modal's content.
                $('.highlight-prof-pic').attr('src', user_photo);
                $('.highlight-user-name').html(user_name +
                    ' <span class="fs-6 fw-light highlight-user-job d-none d-sm-inline-flex"> | ' +
                    user_job + '</span>');
                $('.highlight-user-time').html(time);
                $('.highlight-description').html('<b>' + user_name + '</b> - ' + desc);
                $('.edit-highlight-btn').attr('data-href', edit_href);
                $('.edit-highlight-btn').attr('data-title', title);
                $('.edit-highlight-btn').attr('data-desc', desc);
                $('.delete-highlight-btn').attr('href', delete_href);

                if (is_video == true) {
                    $('.highlight-file-img').addClass('d-none');
                    $('.highlight-file-video').removeClass('d-none');
                    $('.highlight-file-video > source').attr('src', file);
                    $('.highlight-file-video')[0].load();
                } else {
                    $('.highlight-file-video').addClass('d-none');
                    $('.highlight-file-img').removeClass('d-none');
                    $('.highlight-file-img').attr('src', file);
                }
            });
            // End Script View Highlight Item

            // Start Script Preview image
            const imageModal = document.getElementById('imageModal')
            imageModal.addEventListener('show.bs.modal', event => {
                // Button that triggered the modal
                const button = event.relatedTarget

                // Extract info from data-bs-* attributes
                const recipient = button.getAttribute('data-bs-image')

                // If necessary, you could initiate an AJAX request here
                // and then do the updating in a callback.
                //
                // Update the modal's content.
                const modalBodyImage = imageModal.querySelector('.modal-body img')
                modalBodyImage.src = recipient
            })
            // End Script View Preview image

            // Start Script Edit Highlight Item
            $('#editHighlight').on('show.bs.modal', event => {
                // Button that triggered the modal
                const button = event.relatedTarget;

                // Data in Triggered Button
                const href = button.getAttribute('data-href');
                const title = button.getAttribute('data-title');
                const desc = button.getAttribute('data-desc');

                // Update the modal's content.
                $('#editHighlight').find('form').attr('action', href);
                $('#edit-highlight-title').attr('value', title);
                $('#edit-highlight-desc').html(desc);
            });
            // End Script Edit Highlight Item

            // Start Script Delete Highlight Item
            $(".delete-highlight-btn").on("click", function(event) {
                event.preventDefault();

                const href = $(this).attr('href');
                const form = $('#delete-form');

                form.attr('action', href)

                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-md btn-success mx-2',
                        cancelButton: 'btn btn-md btn-danger mx-2',
                    },
                    buttonsStyling: false
                });

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            title: 'Deleted!',
                            text: 'Your highlight has been deleted.',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 2000,
                        });
                        form.submit();
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire({
                            title: 'Cancelled',
                            text: 'Your highlight is safe :v',
                            icon: 'error',
                            showConfirmButton: false,
                            timer: 2000,
                        })
                    }
                });
            });
            // End Script Delete Highlight Item

            // Script for Preview Video in New Video Post Start
            const image_extension = ['png', 'jpg', 'jpeg', 'webp', 'gif', 'svg'];
            const video_extension = ['mp4'];
            const file_extension = image_extension.concat(video_extension);

            $('#typePostVideoX-2').on("change", function(event) {
                let fileName = event.target.value;
                let extension = fileName.substring(fileName.lastIndexOf('.') + 1);

                let validation = video_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                if (validation !== -1) {
                    $(this).closest('.modal-body').find('.preview-placeholder').addClass("d-none");
                    $(this).closest('.modal-body').find('.preview').removeClass("d-none");
                    $(".preview > video > source").attr("src", URL.createObjectURL(event.target.files[0]));
                    $(".preview > video")[0].load();
                }
            });
            // Script for Preview Video in New Video Post End

            // Script for Preview Image in New Image Post or Highlight Start
            $('#typePostImageX-2').on("change", function(event) {
                let element = $(this).closest('.modal');
                let file = element.find('#typePostImageX-2')[0].files[0];
                let fileName = element.find('#typePostImageX-2')[0].value;
                let extension = fileName.substring(fileName.lastIndexOf('.') + 1);
                let reader = new FileReader();

                let validation = image_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                if (validation !== -1) {
                    reader.onloadend = function() {
                        element.find('.preview > img').attr("src", reader.result);
                    }

                    if (file) {
                        reader.readAsDataURL(file);
                        element.find('.preview-placeholder').addClass("d-none");
                        element.find('.preview').removeClass("d-none");
                    } else {
                        // element.find('.profile-img-preview').attr("src", "");
                        element.find('.preview > img').attr("src", "");
                    }
                }
            });

            $('#typeHighlightFileX-2').on("change", function(event) {
                let element = $(this).closest('.modal');
                let file = element.find('#typeHighlightFileX-2')[0].files[0];
                let fileName = element.find('#typeHighlightFileX-2')[0].value;
                let extension = fileName.substring(fileName.lastIndexOf('.') + 1);

                let validation = file_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                let image_validation = image_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                let video_validation = video_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                if (validation !== -1) {
                    if (image_validation !== -1) {
                        let reader = new FileReader();
                        reader.onloadend = function() {
                            element.find('.preview-img > img').attr("src", reader.result);
                        }
                        if (file) {
                            reader.readAsDataURL(file);
                            element.find('.preview-placeholder').addClass("d-none");
                            element.find('.preview-vid').addClass("d-none");
                            element.find('.preview-img').removeClass("d-none");
                        } else {
                            element.find('.preview-img > img').attr("src", "");
                        }
                    } else if (video_validation !== -1) {
                        element.find('.preview-placeholder').addClass("d-none");
                        element.find('.preview-img').addClass("d-none");
                        element.find('.preview-vid').removeClass("d-none");
                        $(".preview-vid > video > source").attr("src", URL.createObjectURL(event.target
                            .files[
                                0]));
                        $(".preview-vid > video")[0].load();
                    }
                }
            });
            // Script for Preview Image in New Image Post or Highlight End

            // Script for Preview Profile and Background in Edit Profile Start
            // $('#typeEditProfileImageX-2').on("change", function(event) {
            //     let element = $(this).closest('.modal');
            //     let value = element.find('#typeEditProfileImageX-2')[0].value;
            //     // let extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            //     // let reader = new FileReader();

            //     // let validation = image_extension.findIndex(element => {
            //     //     if (element.toLowerCase().includes(extension.toLowerCase())) {
            //     //         return true;
            //     //     }
            //     // });

            //     // if (validation !== -1) {
            //     reader.onloadend = function() {
            //         element.find('.profile-img-preview').attr("src", reader.result);
            //     }

            //     if (file) {
            //         reader.readAsDataURL(file);
            //     } else {
            //         element.find('.profile-img-preview').attr("src", "");
            //     }
            //     // }
            // });

            $('#typeEditProfileImageX-2').on("change", function(event) {
                let element = $(this).closest('.modal');
                let file = element.find('#typeEditProfileImageX-2')[0].files[0];
                let fileName = element.find('#typeEditProfileImageX-2')[0].value;
                let extension = fileName.substring(fileName.lastIndexOf('.') + 1);
                let reader = new FileReader();

                let validation = image_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                if (validation !== -1) {
                    reader.onloadend = function() {
                        element.find('.profile-img-preview').attr("src", reader.result);
                    }

                    if (file) {
                        reader.readAsDataURL(file);
                    } else {
                        element.find('.profile-img-preview').attr("src", "");
                    }
                }
            });

            $('#typeEditBackgroundImageX-2').on("change", function(event) {
                let element = $(this).closest('.modal');
                let file = element.find('#typeEditBackgroundImageX-2')[0].files[0];
                let fileName = element.find('#typeEditBackgroundImageX-2')[0].value;
                let extension = fileName.substring(fileName.lastIndexOf('.') + 1);
                let reader = new FileReader();

                let validation = image_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                if (validation !== -1) {
                    reader.onloadend = function() {
                        element.find('.background-img-preview').attr("src", reader.result);
                    }

                    if (file) {
                        reader.readAsDataURL(file);
                    } else {
                        element.find('.background-img-preview').attr("src", "");
                    }
                }
            });
            // Script for Preview Image in Edit Profile End

            // Highlight Slider Start
            var splide = new Splide('.splide', {
                drag: 'free',
                // snap: true,
                pagination: false,
                arrows: false,
                padding: '5rem',
                perPage: 9,
                // mediaQuery: 'min',
                breakpoints: {
                    1399.98: {
                        perPage: 8,
                    },
                    1199.98: {
                        perPage: 7,
                    },
                    991.98: {
                        perPage: 6,
                    },
                    767.98: {
                        perPage: 4,
                    },
                    575.98: {
                        perPage: 3,
                    },
                    424.98: {
                        perPage: 2,
                    },
                },
            });

            splide.mount();
            // Highlight Slider End
        });
    </script>
@endsection
