@extends('layout.app')

@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/post.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> --}}
@endsection

@section('modal')
    <div class="modal fade" id="modal-edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none;">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Post Description
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row h-100">
                        <form action="" method="post" id="edit-form-modal">
                            @csrf
                            @method('PUT')
                            <div class="form-outline mb-4">
                                <label class="form-label" for="textarea-desc">Description</label>
                                <textarea name="desc" class="form-control form-control-md pb-0" id="textarea-desc" rows="10"
                                    placeholder="Type a description for this post" maxlength="800"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer" style="border-top: none;">
                    <button type="submit" class="btn btn-success px-4" form="edit-form-modal">Edit</button>
                </div>
            </div>
        </div>
    </div>

    {{-- PREVIEW IMAGE START --}}
    <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
            <div class="modal-content overflow-hidden">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 d-flex justify-content-center align-items-center">
                    <img src="" alt="" class="w-100 h-100" style="object-fit: contain">
                </div>
            </div>
        </div>
    </div>
    {{-- PREVIEW IMAGE END --}}
@endsection

@section('content')
    @if (isset($posts))
        {{-- Navbar Back to Profile Page Start --}}
        <nav class="navbar bg-light sticky-top">
            <div class="container">
                <a class="navbar-brand d-flex align-items-center text-dark"
                    href="{{ route('profile', $profile->nickname) }}">
                    <i class="fs-4 fa-solid fa-arrow-left"></i>
                    @if (Request::route()->getName() == 'photos')
                        <h4 class="ms-4 mb-0 fw-bold">Photos</h4>
                    @elseif(Request::route()->getName() == 'videos')
                        <h4 class="ms-4 mb-0 fw-bold">Videos</h4>
                    @endif
                </a>
            </div>
        </nav>
        {{-- Navbar Back to Profile Page End --}}

        {{-- Post Box Container Start --}}
        <div class="container gx-0 min-vh-100 d-flex justify-content-center">
            <div class="post-box px-lg-5 px-sm-4 px-0">
                @foreach ($posts as $item)
                    <div class="post-item w-100 my-4" id="post{{ $item->id }}">
                        @if ($item->is_video)
                            <video class="w-100" controls>
                                <source src="{{ asset("img/posts/$item->thumbnail") }}">
                                Your browser does not support HTML video.
                            </video>
                        @else
                            <a href="javascript::void(0)" data-bs-toggle="modal" data-bs-target="#imageModal"
                                data-bs-image="{{ asset("img/posts/$item->thumbnail") }}" class="w-100">
                                <img class="w-100" src="{{ asset("img/posts/$item->thumbnail") }}" alt="Post1">
                            </a>
                        @endif
                        <p class="mb-0 fw-light text-end post-watermark pe-2 pe-sm-0">
                            Work at
                            <span class="text-purple fw-bold">BURNINGROOM TECHNOLOGY</span>
                        </p>
                        <div class="post-description pt-3 px-3 px-md-0">
                            <div class="row gx-0">
                                <div class="col-11 d-flex">
                                    <a href="javascript::void(0)" data-bs-toggle="modal" data-bs-target="#imageModal"
                                        data-bs-image="{{ asset("img/profiles/$profile->photo") }}"
                                        class="post-prof-pic overflow-hidden rounded-circle">
                                        <img class="w-100" src="{{ asset("img/profiles/$profile->photo") }}" alt="Denas"
                                            style="object-fit: cover;">
                                    </a>
                                    <div class="ms-3">
                                        <h5 class="pb-0 mb-0 post-user-name">{{ $item->user->name }} <span
                                                class="fs-6 fw-light post-user-job d-none d-sm-inline-flex">|
                                                {{ $item->user->job }}</span></h5>
                                        <p class="pt-0 mt-0 fw-light post-user-time">
                                            {{ $item->created_at->format('d F Y') }}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-1 d-flex align-items-start justify-content-end">
                                    @auth
                                        @if (Auth::user()->nickname === $profile->nickname)
                                            <div class="btn-group dropstart">
                                                <button type="button" data-bs-toggle="dropdown" aria-expanded="false"
                                                    class="border-0 bg-transparent text-dark">
                                                    <i class="fa-solid fa-ellipsis-vertical"></i>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item text-purple" href="#1"
                                                            data-bs-toggle="modal" data-bs-target="#modal-edit"
                                                            data-desc="{{ $item->desc }}"
                                                            data-href="{{ route('posts.update', $item->id) }}">
                                                            <i class="fa-solid fa-pen pe-2"></i> Edit this Post
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item delete-post text-purple" href="#"
                                                            data-href="{{ route('posts.destroy', $item->id) }}">
                                                            <i class="fa-solid fa-trash pe-2"></i> Delete this Post
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    @endauth
                                </div>
                            </div>
                            <p class="post-description-text">{{ $item->desc }}</p>
                        </div>
                        <hr>
                    </div>
                @endforeach

                {{-- Footer Start --}}
                {{-- <div class="footer text-center mb-4">
						<h6>Copyright {{ date('Y') }}, All rights reserved</h6>
					</div> --}}
                {{-- Footer End --}}
            </div>
        </div>
        {{-- Post Box Container End --}}

        <form action="" method="POST" id="delete-form">
            @csrf
            @method('DELETE')
        </form>
    @else
        <div class="container min-vh-100 d-flex justify-content-center align-items-center">
            <h2 class="text-center">Sorry, User not found !!</h2>
        </div>
    @endif
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            // let getMeTo = $("#{{ $idpost }}");
            // var position = getMeTo.offset().top - container.offset().top + container.scrollTop();
            const getMeTo = document.getElementById('{{ $idpost }}');
            // scrollTo.scrollIntoView({
            //     behavior: 'smooth'
            // }, false);
            const offset = 50;
            const bodyRect = document.body.getBoundingClientRect().top;
            const elementRect = getMeTo.getBoundingClientRect().top;
            const elementPosition = elementRect - bodyRect;
            const offsetPosition = elementPosition - offset;
            // var position = scrollTo.offset().top - container.offset().top + container.scrollTop();
            setTimeout(function() {
                getMeTo.scrollIntoView(true);
                // window.scrollTo({
                //     // top: getMeTo,
                //     top: offsetPosition,
                //     // behavior: 'smooth'
                // });
                // container.animate({
                //     scrollTop: position
                // });
            }, 2500);

            $(".edit-post-btn").on("click", function(event) {
                $(".close-edit-modal").click();
                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: 'Your post has been edited!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(function() {
                    $("#edit-form").submit();
                });
            });

            $(".delete-post").on("click", function(event) {
                event.preventDefault();

                const href = $(this).data('href');
                const form = $('#delete-form');

                form.attr('action', href)

                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-md btn-success mx-2',
                        cancelButton: 'btn btn-md btn-danger mx-2',
                    },
                    buttonsStyling: false
                });

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            title: 'Deleted!',
                            text: 'Your post has been deleted.',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 2000,
                        });
                        form.submit();
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire({
                            title: 'Cancelled',
                            text: 'Your post is safe :v',
                            icon: 'error',
                            showConfirmButton: false,
                            timer: 2000,
                        })
                    }
                });
            });

            // Start Script Preview image
            const imageModal = document.getElementById('imageModal')
            imageModal.addEventListener('show.bs.modal', event => {
                // Button that triggered the modal
                const button = event.relatedTarget

                // Extract info from data-bs-* attributes
                const recipient = button.getAttribute('data-bs-image')

                // If necessary, you could initiate an AJAX request here
                // and then do the updating in a callback.
                //
                // Update the modal's content.
                const modalBodyImage = imageModal.querySelector('.modal-body img')
                modalBodyImage.src = recipient
            })
            // End Script View Preview image

            const modal = $('#modal-edit')
            modal.on('show.bs.modal', event => {
                // Button that triggered the modal
                const button = event.relatedTarget

                const desc = button.getAttribute('data-desc')
                const href = button.getAttribute('data-href')
                // Update the modal's content.
                // const modalTitle = exampleModal.querySelector('.modal-title')
                const textarea = modal.find('#textarea-desc')
                const form = modal.find('#edit-form-modal')

                form.attr('action', href);

                textarea.html(desc)
                // modalBodyInput.value = recipient
            });
        });
    </script>
@endsection
