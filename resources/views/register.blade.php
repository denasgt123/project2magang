@extends('layout.app')

@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
    <div class="container min-vh-100 d-flex align-items-center justify-content-center">
        <div class="row d-flex justify-content-center align-items-center w-100">
            <div class="col-xl-7 col-lg-6 col-md-4 col-12 px-5 d-none d-lg-block ">
                <img class="w-100 my-5 pe-5" src="{{ asset('img/actur besar.png') }}" alt="Logo Acture">
                <h4 class="fw-normal">Aplikasi untuk profile karyawan Burningroom dengan tampilan sosial media.</h4>
            </div>
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="card login-box" style="border-radius: 1rem;">
                    <div class="card-body px-5">
                        <form action="#" method="post" id="regisform">
                            <h2 class="my-4 fw-bold">Sign up</h2>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeNameX-2">Name <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="text" name="name" id="typeNameX-2" class="form-control form-control-md" maxlength="30"
                                    autofocus />
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeNickX-2">Username <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="text" name="nickname" id="typeNickX-2"
                                    class="form-control form-control-md" maxlength="30"/>
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeJobX-2">Job <span
                                        class="text-danger fw-bold">*</span></label>
                                <select name="job" id="typeJobX-2" class="form-select form-select-md">
                                    <option value="Junior Programmer">Junior Programmer</option>
                                    <option value="Senior Programmer">Senior Programmer</option>
                                    <option value="Marketing">Marketing</option>
                                    <option value="Manager">Manager</option>
                                    <option value="UI/UX Desainer">UI/UX Desainer</option>
                                </select>
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeEmailX-2">Email <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="email" name="email" id="typeEmailX-2"
                                    class="form-control form-control-md" />
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typePasswordX-2">Password <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="password" name="password" id="typePasswordX-2"
                                    class="form-control form-control-md" />
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typePassword2X-2">Retype Password <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="password" name="password_confirmation" id="typePassword2X-2"
                                    class="form-control form-control-md" />
                            </div>

                            <div class="row justify-content-center py-4">
                                <button class="btn btn-primary btn-lg px-4 rounded-pill" type="submit">Sign Up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
