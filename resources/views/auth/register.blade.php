{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layout.app')

@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
    <div class="container min-vh-100 d-flex align-items-center justify-content-center mb-5 mb-lg-0">
        <div class="row d-flex justify-content-center align-items-center w-100">
            <div class="col-xl-7 col-lg-6 col-md-4 col-12 px-5 d-none d-lg-block ">
                <img class="w-100 my-5 pe-5" src="{{ asset('img/actur besar.png') }}" alt="Logo Acture">
                <h4 class="fw-normal">Aplikasi untuk profile karyawan Burningroom dengan tampilan sosial media.</h4>
            </div>
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="w-100 my-5 d-flex justify-content-center align-items-center d-lg-none">
                    <img src="{{ asset('img/actur besar.png') }}" alt="Logo Acture" style="height: 5rem; width: auto;">
                </div>
                <div class="card login-box neumorph-light bg-main" style="border-radius: 1rem;">
                    <div class="card-body px-5">
                        <form method="POST" action="{{ route('register') }}" id="regisform">
                            @csrf
                            <h2 class="my-4 fw-bold">Sign up</h2>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeNameX-2">Name <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="text" name="name" id="typeNameX-2"
                                    class="form-control form-control-md @error('name') is-invalid @enderror" autofocus
                                    value="{{ old('name') }}" maxlength="30" />

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeNickX-2">Username <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="text" name="nickname" id="typeNickX-2"
                                    class="form-control form-control-md @error('nickname') is-invalid @enderror"
                                    value="{{ old('nickname') }}" maxlength="30" />

                                @error('nickname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeJobX-2">Job <span
                                        class="text-danger fw-bold">*</span></label>
                                <input name="job" type="text" id="typeJobX-2"
                                    class="form-control form-control-md @error('job') is-invalid @enderror"
                                    placeholder="My Job" value="{{ old('job') }}" maxlength="50" />
                                {{-- <select name="job" id="typeJobX-2"
                                    class="form-select form-select-md @error('job') is-invalid @enderror">
                                    <option value="Junior Programmer">Junior Programmer</option>
                                    <option value="Senior Programmer">Senior Programmer</option>
                                    <option value="Marketing">Marketing</option>
                                    <option value="Manager">Manager</option>
                                    <option value="UI/UX Desainer">UI/UX Desainer</option>
                                </select> --}}

                                @error('job')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typeEmailX-2">Email <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="email" name="email" id="typeEmailX-2"
                                    class="form-control form-control-md @error('email') is-invalid @enderror" />

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typePasswordX-2">Password <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="password" name="password" id="typePasswordX-2"
                                    class="form-control form-control-md @error('password') is-invalid @enderror" />

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label class="form-label" for="typePassword2X-2">Retype Password <span
                                        class="text-danger fw-bold">*</span></label>
                                <input type="password" name="password_confirmation" id="typePassword2X-2"
                                    class="form-control form-control-md @error('password_confirmation') is-invalid @enderror" />

                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="row justify-content-center py-4">
                                <button class="btn btn-primary btn-lg px-4 rounded-pill" type="submit">Sign Up</button>
                            </div>
                        </form>

                        <p class="text-center my-4">Already have an account? <a data-toggle="tab"
                                href="{{ route('login') }}">Sign
                                In Now</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center py-4 d-block">
        <h6>Copyright {{ date('Y') }}, All rights reserved</h6>
    </div>
@endsection

@section('script')
@endsection
