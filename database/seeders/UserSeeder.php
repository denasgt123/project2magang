<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$datas = [
			[
				'photo'      => 'denas.jpg',
				'background' => 'background.jpg',
				'name'       => 'Denassyah Nurrohman',
				'nickname'   => Str::slug('Denassyah Nurrohman'),
				'bio'        => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, quibus damas ddqwea quibusdamasddqwea quibusdamasddqweaas',
				'email'      => 'denas@gmail.com',
				'job'        => 'Senior Programmer',
				'password'   => bcrypt('asdasdasd'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'photo'      => 'denas.jpg',
				'background' => 'photo6.jpg',
				'name'       => 'Mukhlis Bara Pamungkas',
				'nickname'   => Str::slug('Mukhlis Bara Pamungkas'),
				'bio'        => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, quibus damas ddqwea quibusdamasddqwea quibusdamasddqweaas',
				'email'      => 'bara@gmail.com',
				'job'        => 'Junior Programmer',
				'password'   => bcrypt('asdasdasd'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
		];

		User::insert($datas);
	}
}
