<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HighlightController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect()->route('login');
});

Route::get('/home', function () {
	return redirect()->route('login');
})->name('home');


// Google Login Route 
Route::get('google/auth/redirect', [LoginController::class, 'redirectToProvider'])->name('google-login');
Route::get('google/auth/callback', [LoginController::class, 'handleProviderCallback'])->name('google-callback');

Route::put('profile/update/{user}', [ProfileController::class, 'update'])->name('profile.update');
Route::get('profile/{nickname}', [ProfileController::class, 'index'])->name('profile');

// Route::get('/post', function () {
// 	return view('post');
// });

// Route::get('/instagram_clone', function () {
// 	return view('instagram_clone');
// });

Auth::routes();

// Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('posts', PostController::class)->except([
		'create', 'index', 'show', 'edit'
	]);

	Route::resource('highlights', HighlightController::class)->except([
		'create', 'index', 'show', 'edit'
	]);
	// Route::post('posts/', [PostController::class, 'store'])->name('posts.store');
	// Route::put('posts/{post}', [PostController::class, 'update'])->name('posts.update');
	// Route::delete('posts/{post}', [PostController::class, 'destory'])->name('posts.destroy');
});

Route::get('photos/{nickname}/{id?}', [PostController::class, 'showPhotos'])->name('photos');
Route::get('videos/{nickname}/{id?}', [PostController::class, 'showVideos'])->name('videos');
